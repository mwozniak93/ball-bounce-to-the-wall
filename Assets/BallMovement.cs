﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class BallMovement : MonoBehaviour
{
	float frameRate;
	Vector2 startPos;
	// Seconds
//	static	float frameDelay = frameRate * 1000;
	// ms
	[SerializeField]
	bool loopTimer = false;
	[SerializeField]
	float ballSpeedX = 10;
	[SerializeField]
	float ballSpeedY = 0;
	[SerializeField]
		float ballMass = 0.1f;
	[SerializeField]
	 static float ballRadius = 15f;
	[SerializeField]
	float ballRestitution = -0.7f;
	[SerializeField]
	 float width = 60;
	[SerializeField]
	 float height = 60;
	bool isMouseDown =false;


	[SerializeField]
	float Cd = 0.47f;
	// Dimensionless
	[SerializeField]
	float rho = 1.22f;
	// kg / m^3
	[SerializeField]
	float A;
	[SerializeField]
	float ag = 9.81f;
	Vector3 throwPower;
	[SerializeField]
	float jumpPower;
	bool isSpacePressed;
	bool isLeftPresses;
	bool isRightPressed;
	[SerializeField]
	float distanceCovered,highScore;
	[SerializeField]
	Text txtDistanceCovered,txtHighScore;
	Vector3 lastPos;
	[SerializeField]
	GameObject goHighScoreEffect, txtHighScoreEffect;
	[SerializeField]
	float timePlayed;

	public float HighScore{ get
		{ return highScore;} 
		set
		{

			float playerHighScoreSave = PlayerPrefs.GetFloat ("HighScore");
			playerHighScoreSave = Mathf.Round (playerHighScoreSave);
//			Debug.Log ("playerHighScoreSave ; " + playerHighScoreSave);
			txtHighScore.text ="HIGHSCORE: " +  playerHighScoreSave.ToString ();
//			Debug.Log("PlayerPrefs.GetFloat: " +PlayerPrefs.GetFloat ("HighScore"));
			if (value <= playerHighScoreSave) {
				return;
			}
			StartCoroutine (HighScoreEffect ());
//			if (value >= playerHighScoreSave) {
				PlayerPrefs.SetFloat ("HighScore", DistanceCovered);
//			}

		
			highScore = value;
			value = Mathf.Round (value);
			txtHighScore.text ="HIGHSCORE: " +  value.ToString ();

		}
	}

	IEnumerator HighScoreEffect(){
		goHighScoreEffect.SetActive (true);
		yield return new WaitForSeconds (0.25f);
		txtHighScoreEffect.SetActive (true);
		yield return new WaitForSeconds (2.5f);
		txtHighScoreEffect.SetActive (false);
		goHighScoreEffect.SetActive (false);
	}


	public float DistanceCovered{ get
		{ return distanceCovered;} 
		set
		{
			distanceCovered = value;
			value = Mathf.Round (value);
			txtDistanceCovered.text ="DISTANCE: " +  value.ToString ();

		}
	}

	void OnMouseDown(){

		isMouseDown = true;
	}
	void OnMouseUp(){

		isMouseDown = false;

	}


//	void SaveScore(){
//		if (DistanceCovered > HighScore) {
//			PlayerPrefs.SetFloat ("HighScore", DistanceCovered);
//		}
//
//	}
	public void ResetHighScore(){
		if (timePlayed >= 15) {
			PlayerPrefs.DeleteAll ();
			timePlayed = 0;
			HighScore = 0;
		}

	}
	void Awake(){

		A= Mathf.PI * ballRadius * ballRadius / (10000);
		frameRate = Time.deltaTime;
	}
	void Start(){

//		Screen.SetResolution (1024, 600,false);
		startPos = transform.position;
		ballRadius = GetComponent<CircleCollider2D> ().radius;
		HighScore = 0;
//		HighScore
		//Debug.Log ("ballRadius ; " + ballRadius);
	}

	void Jump(){
		if (isSpacePressed) {
			jumpPower += Time.deltaTime*-2f;
			ballSpeedY += jumpPower;
		
		} else {
			jumpPower = 0;
		}


		if (isLeftPresses) {
			ballSpeedX += Time.deltaTime * -5f;
		} 	
		if (isRightPressed) {
			ballSpeedX += Time.deltaTime * 5f;
		}


		if (Input.GetKeyDown (KeyCode.Space)) {
			isSpacePressed = true;
		}
		if (Input.GetKeyUp (KeyCode.Space)) {
			isSpacePressed = false;
		}

		if (Input.GetKeyDown (KeyCode.LeftArrow)) {
			isLeftPresses = true;
		}
		if (Input.GetKeyUp (KeyCode.LeftArrow)) {
			isLeftPresses = false;
		}

		if (Input.GetKeyDown (KeyCode.RightArrow)) {
			isRightPressed = true;
		}
		if (Input.GetKeyUp (KeyCode.RightArrow)) {
			isRightPressed = false;
		}



	}
	void SetHighScore(){
		
		HighScore = DistanceCovered;
//		SaveScore ();
		DistanceCovered = 0;
	}

	void Update ()
	{

		timePlayed += Time.deltaTime;
		DistanceCovered  += Vector3.Distance (lastPos, transform.position);
		lastPos = transform.position;
		Jump ();

		if (!isMouseDown) {
			throwPower -= throwPower *Time.deltaTime;

			//calculating physics
			float Fx = -0.5f * Cd * A * rho * ballSpeedX * ballSpeedX * ballSpeedX / Mathf.Abs (ballSpeedX);  
			float Fy = -0.5f * Cd * A * rho * ballSpeedY * ballSpeedY * ballSpeedY / Mathf.Abs (ballSpeedY);
//			Debug.Log ("FX 1: " + Fx);
//			Fx = (double.IsNaN (Fx) ? 0 : Fx);  
//			Fy = (double.IsNaN (Fy) ? 0 : Fy);
			if (double.IsNaN (Fx)) Fx = 0;
			if (double.IsNaN (Fy)) Fy = 0;

//			} else {
//				Fx = Fx;	
//			}

//			Debug.Log ("FX 2: " + Fx);
			//  acceleration
			float ax = Fx / ballMass;  
			float ay = ag + (Fy/ballMass);

			ballSpeedX += ax * frameRate;  
			ballSpeedY += ay * frameRate;

			transform.position = new Vector2 ( transform.position.x +  ballSpeedX * frameRate,  transform.position.y - ballSpeedY * frameRate);


			//Collision detection:
			if (transform.position.y < height - height ) {
				ballSpeedY *= ballRestitution;

				transform.position = new Vector2 (transform.position.x, height - height);
				SetHighScore ();
			}

			if (transform.position.y+ballRadius > height) {
				ballSpeedY *= ballRestitution;
				transform.position = new Vector2 (transform.position.x, height-ballRadius);
				SetHighScore ();
			}

			if (transform.position.x > width - ballRadius) {
				ballSpeedX *= ballRestitution;
				transform.position = new Vector2 (width - ballRadius, transform.position.y);
				SetHighScore ();
			}
			if (transform.position.x < ballRadius) {
				ballSpeedX *= ballRestitution;
				transform.position = new Vector2 (ballRadius, transform.position.y);
				SetHighScore ();
			}
				
		}

		else{
			Vector3 lastPosXY = transform.position;
			Vector2 targetPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
			transform.position = new Vector2 (targetPos.x,targetPos.y);
			throwPower = (transform.position - lastPosXY).normalized;

		}
	}


}
